<?php

/**
 * @file
 * Conatins the code for settings related to cart deletion undo.
 */

/**
 * Settings form to save the substring that will be present in your message
 * after line item is deleted from cart.
 */
function cart_deletion_undo_setting_form($form, $form_state) {
  $form['cart_deletion_undo_substring'] = array(
    '#title' => t('Cart Deletion Undo String'),
    '#description' => t('Substring that will be present in your message after line item is deleted from cart.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('cart_deletion_undo_substring', 'removed from your cart'),
    '#size' => 30,
  );
  return system_settings_form($form);
}
