<?php

/**
 * @file
 * Callback function to undo the deletion of a line item from cart.
 */

/**
 * Implements callback to undo the cart deletion operation.
 */
function cart_deletion_undo() {
  global $user;
  $uid = $user->uid;
  if (isset($_SESSION['deleted_line_item']) && isset($_SESSION['deleted_product'])) {
    $deleted_line_item = $_SESSION['deleted_line_item'];
    $deleted_product = $_SESSION['deleted_product'];
    // Get order id the line item is associated to.
    $deleted_order_id = $deleted_line_item->order_id;
    $deleted_line_item_quantity = $deleted_line_item->quantity;
    $product_load = commerce_product_load($deleted_product->product_id);
    $line_item_new = commerce_product_line_item_new($product_load, $deleted_line_item_quantity, $deleted_order_id);
    commerce_line_item_save($line_item_new);
    // Adding the line item again to cart.
    commerce_cart_product_add($uid, $line_item_new);
    unset($_SESSION['deleted_line_item']);
    unset($_SESSION['deleted_product']);
    drupal_goto('cart');
  }
}
