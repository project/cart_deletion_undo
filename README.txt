CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
 * This module help(s) a user retrieve the line item deleted from his/her cart.
Click on the undo link and the line item will be added back to your cart.


REQUIREMENTS
------------
This module requires the following modules:

 * Drupal Commerce(https://www.drupal.org/project/commerce)


INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 * Go to admin/commerce/config/cart-deletion-undo-setting
 * Enter the substring that will be present in your line item deletion message.
 * Save the form.


MAINTAINER
-----------
Current maintainers:
 * Hardik Pandya (hardik.p) - https://www.drupal.org/user/3220495
